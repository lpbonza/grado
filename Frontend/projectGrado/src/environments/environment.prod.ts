export const environment = {
  production: true,
  url: 'http://127.0.0.1:3012/',
  urlProject: 'http://127.0.0.1:3011/',
  urlParametros: 'http://127.0.0.1:3010/',
};
