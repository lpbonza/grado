import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContratosService {

  constructor(private http: HttpClient) { }

  postContrato(data) {
    return this.http.post<any>(environment.urlProject + 'contratos', data);
  }

  getContrato() {
    return this.http.get<any>(environment.urlProject + 'contratos' );
  }

  getCountContrato(token) {
    return this.http.get<any>(environment.urlProject + 'contratos/' + token);
  }

  getMisContratos(token) {
    return this.http.get<any>(environment.urlProject + 'api/contratos/' + token );
  }

  deleteContrato(token) {
    return this.http.delete<any>(environment.urlProject + 'contratos/' + token);
  }

  putContrato(token, data) {
    return this.http.put<any>(environment.urlProject + 'contratos/' + token, data);
  }

}
