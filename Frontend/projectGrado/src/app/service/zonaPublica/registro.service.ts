import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Registro } from '../../models/zonaPublica/registro/registro';
import { environment } from 'src/environments/environment.prod';
import { Recovery } from 'src/app/models/recovery/recovery';

@Injectable({
  providedIn: 'root'
})
export class RegistroService {
  params: {};

  constructor(private http: HttpClient) { }

  registrar(userData) {
    return this.http.post<Registro>(environment.url + 'api/createusers', userData);
  }

  recovery(userData) {
    return this.http.post<Recovery>(environment.url + 'api/recovery', userData);
  }

  change(userData, key) {
    this.params = {
      userData,
      key
    };
    return this.http.put<Recovery>(environment.url + 'api/change', this.params);
  }

}
