import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  constructor(private http: HttpClient) { }

  getPais() {
    return this.http.get<any>('http://127.0.0.1:3010/pais');
  }

  getDepartamento() {
    return this.http.get<any>('http://127.0.0.1:3010/departamentos');
  }

  getCiudad() {
    return this.http.get<any>('http://127.0.0.1:3010/ciudades');
  }

  getAlmacenContratos() {
    return this.http.get<any>(environment.urlParametros + '​/api​/almacencontratos');
  }

  getCargos() {
    return this.http.get<any>('http://127.0.0.1:3010/api/cargos');
  }

  getClausulas() {
    console.log('Consulta')
    return this.http.get<any>('//127.0.0.1:3010/api/clausulas');
  }

  getParametros() {
    return this.http.get<any>('//127.0.0.1:3010/api/parametros');
  }

  getProfesiones() {
    return this.http.get<any>(environment.urlParametros + '/api/profesiones');
  }

  getIdProfesiones(data) {
    return this.http.get<any>(environment.urlParametros + '/api/profesiones/' + data);
  }

  getSectores() {
    return this.http.get<any>(environment.urlParametros + '/api/sectores');
  }

  getTipoContratos() {
    return this.http.get<any>(environment.urlParametros + '/api/tipocontratos');
  }

  getTipoDocumento() {
    return this.http.get<any>(environment.urlParametros + '/api/tipodocumentos');
  }

  getTipologia() {
    return this.http.get<any>(environment.urlParametros + '/api/tipologias');
  }


  // Counts parametos

  getPaisCount() {
    return this.http.get<any>('http://127.0.0.1:3010/pais/count');
  }

  getDepartamentoCount() {
    return this.http.get<any>('http://127.0.0.1:3010/departamentos/count');
  }

  getCiudadCount() {
    return this.http.get<any>('http://127.0.0.1:3010/ciudades/count');
  }

  getAlmacenContratosCount() {
    return this.http.get<any>(environment.urlParametros + '​/api​/almacencontratos/count');
  }

  getCargosCount() {
    return this.http.get<any>('http://127.0.0.1:3010/api/cargos/count');
  }

  getClausulasCount() {
    return this.http.get<any>(environment.urlParametros + '​/api/clausulas/count');
  }

  getParametrosCount() {
    return this.http.get<any>(environment.urlParametros + '​​/api​/parametros/count');
  }

  getProfesionesCount() {
    return this.http.get<any>(environment.urlParametros + '/api/profesiones/count');
  }

  getSectoresCount() {
    return this.http.get<any>(environment.urlParametros + '/api/sectores/count');
  }

  getTipoContratosCount() {
    return this.http.get<any>(environment.urlParametros + '/api/tipocontratos/count');
  }

  getTipoDocumentoCount() {
    return this.http.get<any>(environment.urlParametros + '/api/tipodocumentos/count');
  }

  getTipologiaCount() {
    return this.http.get<any>(environment.urlParametros + '/api/tipologias/count');
  }

  // consultapor id

  getIdPais(id) {
    return this.http.get<any>('http://127.0.0.1:3010/pais/' + id);
  }

  getIdDepartamento(id) {
    return this.http.get<any>('http://127.0.0.1:3010/departamentos/' + id);
  }

  getIdCiudad(id) {
    return this.http.get<any>('http://127.0.0.1:3010/ciudades/' + id);
  }


}
