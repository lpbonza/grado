import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserCountService {

  constructor(private cookie: CookieService,
              private http: HttpClient) { }

  authCount(){
    return JSON.parse(this.cookie.get('userAcount'));
  }
}
