import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { NaturalService } from 'src/app/service/zonaPrivada/personas/natural.service';
import { ParametrosService } from 'src/app/service/parametros/parametros.service';
import { ContratosService } from 'src/app/service/personas/contratos/contratos.service';
import { Contratos } from 'src/app/models/zonaPrivada/contratos';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.less']
})
export class PanelComponent implements OnInit {
  showFiller = true;
  userLogin: any;
  infoUserLoged: any;
  profesion: any;
  contratosCuentas: any;
  countPendientes: any;
  pendientes = [];
  finalizado = [];
  pendientesCounter: number;
  finalizadoCounter: number;
  misContratos: any;
  

  constructor(private cookie: CookieService,
              private naturaService: NaturalService,
              private contratosService: ContratosService,
              private parametrosServices: ParametrosService) {
    this.userLogin = JSON.parse(this.cookie.get('userAcount'));
    console.log(this.userLogin);
    // idUsers
    this.naturaService.getPersonaNatural(this.userLogin.idUsers)
      .subscribe(data=>{
        this.infoUserLoged = data;
        console.log(this.infoUserLoged)
        this.parametrosServices.getIdProfesiones(this.infoUserLoged.Profesion_idProfesion)
          .subscribe(data => {
            this.profesion = data;
            this.cookie.set('Profesion', this.profesion.Profesion);
            console.log(this.profesion)
          }, err => {
            alert('No podemos obtener su profesión');
          });
      }, err => {
        this.naturaService.getPersonaJuridica(this.userLogin.idUsers)
          .subscribe(data => {
            this.infoUserLoged = data;
            console.log(this.infoUserLoged)
          }, err => {
            console.log(err);
            alert('No podemos traer sus datos');
          });
      });
  }

  ngOnInit() {
    this.contratosService.getMisContratos(this.userLogin.idUsers).subscribe(data => {
      this.contratosCuentas = data;
      console.log(this.contratosCuentas)
      for(let filtro of this.contratosCuentas) {
        if(filtro.EstadoContrato_idEstadoContrato === 1){
          this.pendientes.push(filtro);
        } else if(filtro.EstadoContrato_idEstadoContrato === 2){
          this.finalizado.push(filtro);
        }
      }
      this.pendientesCounter = this.pendientes.length;
      this.finalizadoCounter = this.finalizado.length;
      console.log(this.pendientes)
      console.log(this.finalizado)
      this.misContratos = data.length;
    }, err => {
      this.misContratos = 0;
      this.pendientesCounter = 0;
      this.finalizadoCounter = 0;
    })

  }
}
