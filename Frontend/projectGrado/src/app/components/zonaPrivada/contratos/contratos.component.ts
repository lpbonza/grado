import { CookieService } from 'ngx-cookie-service';
import { LoginService } from 'src/app/service/zonaPublica/login.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.less']
})
export class ContratosComponent implements OnInit {
  persona: FormGroup;
  
  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private cookie: CookieService) { }

  ngOnInit() {
    this.persona = this.fb.group({
      idPersona: ['', [Validators.required]],
      Nombre: ['', [Validators.required]],
      Apellido: ['', [Validators.required]],
      Direccion: ['', [Validators.required]],
      FirmaContratoPersona: ['', [Validators.required]],
      Profesion_idProfesion: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]]
    });
  }

}
