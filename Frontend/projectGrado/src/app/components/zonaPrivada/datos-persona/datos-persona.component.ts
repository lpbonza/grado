import { ParametrosService } from './../../../service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { NaturalService } from 'src/app/service/zonaPrivada/personas/natural.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-datos-persona',
  templateUrl: './datos-persona.component.html',
  styleUrls: ['./datos-persona.component.less']
})
export class DatosPersonaComponent implements OnInit {
  persona: FormGroup;
  empresas: FormGroup;
  contactos: FormGroup;
  firmanteForm: FormGroup;
  type: boolean = true;
  natural: boolean = false;
  juridica: boolean = false;
  contacto: boolean = false;
  firmante: boolean = false;
  profeciones: any;
  pais: any;
  departamento: any;
  ciudad: any;
  tipologias: any;
  sectores: any;
  tiposDocumentos: any;
  cargos: any;
  Parametros: any;

  constructor(private fb: FormBuilder,
              private naturalService: NaturalService,
              private router: Router,
              private parametrosService: ParametrosService,
              private cookie: CookieService) {
                if (this.cookie.check('valor')) {
                  if(this.cookie.get('valor') === '1') {
                    this.natural = true;
                    this.type = false;
                  } else {
                    this.juridica = true;
                    this.type = false;
                  }
                }
                this.parametrosService.getProfesiones().subscribe(data => this.profeciones = data);
                this.parametrosService.getPais().subscribe(data => this.pais = data);
                this.parametrosService.getDepartamento().subscribe(data => this.departamento = data);
                this.parametrosService.getCiudad().subscribe(data => this.ciudad = data);
                this.parametrosService.getSectores().subscribe(data => this.sectores = data);
                this.parametrosService.getTipologia().subscribe(data => this.tipologias = data);
                this.parametrosService.getTipoDocumento().subscribe(data => this.tiposDocumentos = data);
                this.parametrosService.getCargos().subscribe(data => this.cargos = data);
              }

  ngOnInit() {
    this.persona = this.fb.group({
      idPersona: ['', [Validators.required]],
      Nombre: ['', [Validators.required]],
      Apellido: ['', [Validators.required]],
      Direccion: ['', [Validators.required]],
      FirmaContratoPersona: ['', [Validators.required]],
      Profesion_idProfesion: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]]
    });
    this.empresas = this.fb.group({
      nitEmpresa: ['', [Validators.required]],
      Nombre: ['', [Validators.required]],
      numeroEmpleados: ['', [Validators.required]],
      Direccion: ['', [Validators.required]],
      Pagina: ['', [Validators.required]],
      Sector_idSector: ['', [Validators.required]],
      Tipologia_idTipologia: ['', [Validators.required]],
      Pais_idPais: ['', [Validators.required]],
      Departamento_idDepartamento: ['', [Validators.required]],
      Ciudad_idCiudad: ['', [Validators.required]]
    });
    this.contactos = this.fb.group({
      IdContacto: ['', [Validators.required]],
      Nombre: ['', [Validators.required]],
      Apellido: ['', [Validators.required]],
      Email: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]],
      Cargo_idCargo: ['', [Validators.required]],
      Empresa_nitEmpresa: ['', [Validators.required]]
    });
    this.firmanteForm = this.fb.group({
      Documento: ['', [Validators.required]],
      Nombre: ['', [Validators.required]],
      Apellidos: ['', [Validators.required]],
      FirmaContrato: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]],
      Empresa_nitEmpresa: ['', [Validators.required]]
    });
    const data = JSON.parse(this.cookie.get('userAcount'));
    if (!this.natural) {
      this.empresas.controls['nitEmpresa'].setValue(data.idUsers);
      this.firmanteForm.controls['Empresa_nitEmpresa'].setValue(data.idUsers);
      this.contactos.controls['Empresa_nitEmpresa'].setValue(data.idUsers);
      // this.empresas.controls['TipoDocumento_idTipoDocumento'].setValue(data.TipoDocumento_idTipoDocumento);
    } else {
      this.persona.controls['idPersona'].setValue(data.idUsers);
      this.persona.controls['TipoDocumento_idTipoDocumento'].setValue(data.TipoDocumento_idTipoDocumento);
    }
  }

  registerPerson(event) {
    this.cookie.set('valor', event);
    if (event === 1) {
      this.natural = true;
      this.type = false;
    } else {
      this.juridica = true;
      this.type = false;
    }
  }

  onSubmit() {
    this.naturalService.postPersonaNatural(this.persona.value)
      .subscribe(data => {
        this.router.navigate(['/panel']);
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  onSubmitJuridica() {
    this.naturalService.postPersonaJuridica(this.empresas.value)
      .subscribe(data => {
        this.juridica = false;
        this.contacto = true;
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  onSubmitContacto() {
    this.naturalService.postPersonaContacto(this.contactos.value)
      .subscribe(data => {
        this.contacto = false;
        this.firmante = true;
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

  onSubmitFirmante() {
    this.naturalService.postPersonaFirmante(this.firmanteForm.value)
      .subscribe(data => {
        this.router.navigate(['/panel'])
      }, err => {
        alert('Por favor contactese con el administrador');
        console.log(err);
      });
  }

}
