import { ParametrosService } from './../../../service/parametros/parametros.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.less']
})
export class AdminComponent implements OnInit {
  documentos: any;
  pais: any;
  departamentos: any;
  ciudad: any;
  tipologia: any;

  constructor(private parametrosService: ParametrosService) { }

  ngOnInit() {
    this.parametrosService.getTipoDocumentoCount().subscribe(data => this.documentos = data)
    this.parametrosService.getPaisCount().subscribe(data => this.pais = data)
    this.parametrosService.getDepartamentoCount().subscribe(data => this.departamentos = data)
    this.parametrosService.getCiudadCount().subscribe(data => this.ciudad = data)
    this.parametrosService.getTipologiaCount().subscribe(data => this.tipologia = data)
  }

}
