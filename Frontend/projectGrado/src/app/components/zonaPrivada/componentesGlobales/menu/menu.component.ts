import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {
  profesiones: string;

  constructor(private cookie: CookieService) {
    this.profesiones = this.cookie.get('Profesion')
  }

  ngOnInit() {
  }

}
