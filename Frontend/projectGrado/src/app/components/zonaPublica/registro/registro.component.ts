import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegistroService } from 'src/app/service/zonaPublica/registro.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.less']
})
export class RegistroComponent implements OnInit {
  registro: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private registroService: RegistroService) { }

  ngOnInit() {
    this.registro = this.fb.group({
      idUsers: ['', [Validators.required]],
      Email: ['', [Validators.required, Validators.email]],
      Name: ['', [Validators.required]],
      Password: ['', [Validators.required]],
      Celular: ['', [Validators.required]],
      TipoDocumento_idTipoDocumento: ['', [Validators.required]],
      Roles_idRoles: ['2']
    });
  }

  onSubmit() {
    if (this.registro.valid) {
      this.registroService.registrar(this.registro.value)
        .subscribe(data => {
          alert('Se crea Cuenta correctamente');
          this.router.navigate(['/login']);
        }, err => {
          alert('No se puede registrar usuario');
        });
    }
  }

}
