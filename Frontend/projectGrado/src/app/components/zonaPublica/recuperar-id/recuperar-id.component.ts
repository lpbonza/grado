import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistroService } from 'src/app/service/zonaPublica/registro.service';


@Component({
  selector: 'app-recuperar-id',
  templateUrl: './recuperar-id.component.html',
  styleUrls: ['./recuperar-id.component.less']
})
export class RecuperarIdComponent implements OnInit {
  change: FormGroup;
  key: String;

  constructor(private fb: FormBuilder,
              private router: Router,
              private _route: ActivatedRoute,
              private registroService: RegistroService) { }

  ngOnInit() {
    this.change = this.fb.group({
      Password: ['', [Validators.required]],
    });
  }

  onSubmit() {
    if (this.change.valid) {
      this.key = this._route.snapshot.paramMap.get('id');
      console.log(this.key);
      this.registroService.change(this.change.value, this.key)
        .subscribe(data => {
          this.router.navigate(['/login']);
        }, err => {
          alert('No se puede registrar usuario');
        });
    }
  }

}
