import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './componets/publiczone/index/index.component';
import { RegisterComponent } from './componets/publiczone/register/register.component';
import { LoginComponent } from './componets/publiczone/login/login.component';
import { RecoveryComponent } from './componets/publiczone/recovery/recovery.component';
import { ChangeComponent } from './componets/publiczone/change/change.component';


const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'sing-up',
    component: RegisterComponent
  },
  {
    path: 'sing-in',
    component: LoginComponent
  },
  {
    path: 'recovery',
    component: RecoveryComponent
  },
  {
    path: 'change',
    component: ChangeComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
