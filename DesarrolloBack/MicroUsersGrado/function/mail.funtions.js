"use strict";
const nodemailer = require("nodemailer");
const env = require('dotenv');


const mailService = {}

mailService.sendMail = (mail, type, key) => {
  let testAccount = nodemailer.createTestAccount();
  let transporter = nodemailer.createTransport({
    pool: true,
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: "cooperativasapp@gmail.com",
      pass: "1602003139Odin!"
    }
  });
  transporter.verify(function(error, success) {
    if (error) {
      console.log(error);
    } else {
      console.log("Server is ready to take our messages");
    }
  });
  let info;
  if (type === 'registro') {
    info = transporter.sendMail({
      from: '"Unipanamericana 👻" <cooperativasapp@gmail.com>',
      to: mail,
      subject: "Bienvenido a tu cuenta ✔",
      text: "Hola Bienvenido a tus contratos",
    });
  } else if (type === 'Recuperar') {
    info = transporter.sendMail({
      from: '"Unipanamericana 👻" <cooperativasapp@gmail.com>',
      to: mail,
      subject: "Recuperar cuenta Unipanamericana ✔",
      text: "Hola estamos a punto de terminar, ingresa en el siguiente link:",
      html: "Hola estamos a punto de terminar, ingresa en el siguiente link: <a href='http://localhost:4200/#/change/" + key + "'>Recuperar mi contraseña</a>"
    });
  } else if (type === 'Change') {
    info = transporter.sendMail({
      from: '"Unipanamericana 👻" <cooperativasapp@gmail.com>',
      to: mail,
      subject: "Se realizo un proceso en tu cuenta ✔",
      text: "Se realizo un cambio de contraseña de tu cuenta",
      html: "<a href='http://localhost:4200/#/'>Ir al sitio</a>"
    });
  }
  
  console.log("Message sent: %s", info.messageId);
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  
  return true
}

module.exports = mailService