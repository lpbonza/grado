const Sequelize = require('sequelize')
const sequelize = require('../config/settings.config')

const User = sequelize.define('Users', {
    idPermissions: {type: Sequelize.SMALLINT, primaryKey: true},
    Permissions: Sequelize.STRING
},
{
    timestamps: false
})

module.exports = User