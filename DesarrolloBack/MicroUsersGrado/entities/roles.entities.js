const Sequelize = require('sequelize')
const sequelize = require('../config/settings.config')

const User = sequelize.define('Users', {
    idRoles: {type: Sequelize.SMALLINT, primaryKey: true},
    Rol: Sequelize.STRING
},
{
    timestamps: false
})

module.exports = User