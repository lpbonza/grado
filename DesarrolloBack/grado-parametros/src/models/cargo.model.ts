import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Cargo extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idCargo?: number;

  @property({
    type: 'string',
    required: true,
  })
  Cargo: string;


  constructor(data?: Partial<Cargo>) {
    super(data);
  }
}

export interface CargoRelations {
  // describe navigational properties here
}

export type CargoWithRelations = Cargo & CargoRelations;
