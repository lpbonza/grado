import {Entity, model, property} from '@loopback/repository';

@model()
export class Departamento extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
    generated: false,
  })
  idDepartamento: number;

  @property({
    type: 'string',
    required: true,
  })
  Departamento: string;

  @property({
    type: 'number',
    required: true,
  })
  Pais_idPais: number;


  constructor(data?: Partial<Departamento>) {
    super(data);
  }
}

export interface DepartamentoRelations {
  // describe navigational properties here
}

export type DepartamentoWithRelations = Departamento & DepartamentoRelations;
