import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class TipoContrato extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idTipoContrato?: number;

  @property({
    type: 'string',
    required: true,
  })
  TipoContrato: string;

  @property({
    type: 'string',
    required: true,
  })
  Descripcion: string;


  constructor(data?: Partial<TipoContrato>) {
    super(data);
  }
}

export interface TipoContratoRelations {
  // describe navigational properties here
}

export type TipoContratoWithRelations = TipoContrato & TipoContratoRelations;
