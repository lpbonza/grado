import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Sector extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idSector?: number;

  @property({
    type: 'string',
    required: true,
  })
  Sector: string;


  constructor(data?: Partial<Sector>) {
    super(data);
  }
}

export interface SectorRelations {
  // describe navigational properties here
}

export type SectorWithRelations = Sector & SectorRelations;
