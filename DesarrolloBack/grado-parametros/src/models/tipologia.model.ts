import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Tipologia extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idTipologia?: number;

  @property({
    type: 'string',
    required: true,
  })
  Tipologia: string;


  constructor(data?: Partial<Tipologia>) {
    super(data);
  }
}

export interface TipologiaRelations {
  // describe navigational properties here
}

export type TipologiaWithRelations = Tipologia & TipologiaRelations;
