import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class AlmacenContratos extends Entity {
  @property({
    type: 'number',
    id: true,
  })
  idAlmacenContratos?: number;

  @property({
    type: 'string',
    required: true,
  })
  AlmacenContratos: string;

  @property({
    type: 'number',
    required: true,
  })
  TipoContrato_idTipoContrato: number;


  constructor(data?: Partial<AlmacenContratos>) {
    super(data);
  }
}

export interface AlmacenContratosRelations {
  // describe navigational properties here
}

export type AlmacenContratosWithRelations = AlmacenContratos & AlmacenContratosRelations;
