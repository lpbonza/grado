import {DefaultCrudRepository} from '@loopback/repository';
import {TipoContrato, TipoContratoRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TipoContratoRepository extends DefaultCrudRepository<
  TipoContrato,
  typeof TipoContrato.prototype.idTipoContrato,
  TipoContratoRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(TipoContrato, dataSource);
  }
}
