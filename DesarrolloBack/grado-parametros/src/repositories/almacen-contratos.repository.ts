import {DefaultCrudRepository} from '@loopback/repository';
import {AlmacenContratos, AlmacenContratosRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AlmacenContratosRepository extends DefaultCrudRepository<
  AlmacenContratos,
  typeof AlmacenContratos.prototype.idAlmacenContratos,
  AlmacenContratosRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(AlmacenContratos, dataSource);
  }
}
