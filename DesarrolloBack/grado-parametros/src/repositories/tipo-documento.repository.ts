import {DefaultCrudRepository} from '@loopback/repository';
import {TipoDocumento, TipoDocumentoRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TipoDocumentoRepository extends DefaultCrudRepository<
  TipoDocumento,
  typeof TipoDocumento.prototype.idTipoDocumento,
  TipoDocumentoRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(TipoDocumento, dataSource);
  }
}
