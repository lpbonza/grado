import {DefaultCrudRepository} from '@loopback/repository';
import {Ciudad, CiudadRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CiudadRepository extends DefaultCrudRepository<
  Ciudad,
  typeof Ciudad.prototype.idCiudad,
  CiudadRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Ciudad, dataSource);
  }
}
