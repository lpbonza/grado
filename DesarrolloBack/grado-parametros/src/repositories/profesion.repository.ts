import {DefaultCrudRepository} from '@loopback/repository';
import {Profesion, ProfesionRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ProfesionRepository extends DefaultCrudRepository<
  Profesion,
  typeof Profesion.prototype.idProfesion,
  ProfesionRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Profesion, dataSource);
  }
}
