import {DefaultCrudRepository} from '@loopback/repository';
import {Tipologia, TipologiaRelations} from '../models';
import {MyMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TipologiaRepository extends DefaultCrudRepository<
  Tipologia,
  typeof Tipologia.prototype.idTipologia,
  TipologiaRelations
> {
  constructor(
    @inject('datasources.MyMysql') dataSource: MyMysqlDataSource,
  ) {
    super(Tipologia, dataSource);
  }
}
