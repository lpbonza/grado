import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { TipoDocumento } from '../models';
import { TipoDocumentoRepository } from '../repositories';

export class TipoDocumentoController {
  constructor(
    @repository(TipoDocumentoRepository)
    public tipoDocumentoRepository: TipoDocumentoRepository,
  ) { }

  @post('/api/tipodocumentos', {
    responses: {
      '200': {
        description: 'TipoDocumento model instance',
        content: { 'application/json': { schema: getModelSchemaRef(TipoDocumento) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TipoDocumento, { exclude: ['idTipoDocumento'] }),
        },
      },
    })
    tipoDocumento: Omit<TipoDocumento, 'idTipoDocumento'>,
  ): Promise<TipoDocumento> {
    return this.tipoDocumentoRepository.create(tipoDocumento);
  }

  @get('/api/tipodocumentos/count', {
    responses: {
      '200': {
        description: 'TipoDocumento model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(TipoDocumento)) where?: Where<TipoDocumento>,
  ): Promise<Count> {
    return this.tipoDocumentoRepository.count(where);
  }

  @get('/api/tipodocumentos', {
    responses: {
      '200': {
        description: 'Array of TipoDocumento model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(TipoDocumento) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(TipoDocumento)) filter?: Filter<TipoDocumento>,
  ): Promise<TipoDocumento[]> {
    return this.tipoDocumentoRepository.find(filter);
  }

  @patch('/api/tipodocumentos', {
    responses: {
      '200': {
        description: 'TipoDocumento PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TipoDocumento, { partial: true }),
        },
      },
    })
    tipoDocumento: TipoDocumento,
    @param.query.object('where', getWhereSchemaFor(TipoDocumento)) where?: Where<TipoDocumento>,
  ): Promise<Count> {
    return this.tipoDocumentoRepository.updateAll(tipoDocumento, where);
  }

  @get('/api/tipodocumentos/{id}', {
    responses: {
      '200': {
        description: 'TipoDocumento model instance',
        content: { 'application/json': { schema: getModelSchemaRef(TipoDocumento) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<TipoDocumento> {
    return this.tipoDocumentoRepository.findById(id);
  }

  @patch('/api/tipodocumentos/{id}', {
    responses: {
      '204': {
        description: 'TipoDocumento PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TipoDocumento, { partial: true }),
        },
      },
    })
    tipoDocumento: TipoDocumento,
  ): Promise<void> {
    await this.tipoDocumentoRepository.updateById(id, tipoDocumento);
  }

  @put('/api/tipodocumentos/{id}', {
    responses: {
      '204': {
        description: 'TipoDocumento PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() tipoDocumento: TipoDocumento,
  ): Promise<void> {
    await this.tipoDocumentoRepository.replaceById(id, tipoDocumento);
  }

  @del('/api/tipodocumentos/{id}', {
    responses: {
      '204': {
        description: 'TipoDocumento DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.tipoDocumentoRepository.deleteById(id);
  }
}
