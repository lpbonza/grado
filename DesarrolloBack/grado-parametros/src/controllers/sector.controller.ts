import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Sector } from '../models';
import { SectorRepository } from '../repositories';

export class SectorController {
  constructor(
    @repository(SectorRepository)
    public sectorRepository: SectorRepository,
  ) { }

  @post('/api/sectores', {
    responses: {
      '200': {
        description: 'Sector model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Sector) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Sector, { exclude: ['idSector'] }),
        },
      },
    })
    sector: Omit<Sector, 'idSector'>,
  ): Promise<Sector> {
    return this.sectorRepository.create(sector);
  }

  @get('/api/sectores/count', {
    responses: {
      '200': {
        description: 'Sector model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Sector)) where?: Where<Sector>,
  ): Promise<Count> {
    return this.sectorRepository.count(where);
  }

  @get('/api/sectores', {
    responses: {
      '200': {
        description: 'Array of Sector model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Sector) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Sector)) filter?: Filter<Sector>,
  ): Promise<Sector[]> {
    return this.sectorRepository.find(filter);
  }

  @patch('/api/sectores', {
    responses: {
      '200': {
        description: 'Sector PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Sector, { partial: true }),
        },
      },
    })
    sector: Sector,
    @param.query.object('where', getWhereSchemaFor(Sector)) where?: Where<Sector>,
  ): Promise<Count> {
    return this.sectorRepository.updateAll(sector, where);
  }

  @get('/api/sectores/{id}', {
    responses: {
      '200': {
        description: 'Sector model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Sector) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Sector> {
    return this.sectorRepository.findById(id);
  }

  @patch('/api/sectores/{id}', {
    responses: {
      '204': {
        description: 'Sector PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Sector, { partial: true }),
        },
      },
    })
    sector: Sector,
  ): Promise<void> {
    await this.sectorRepository.updateById(id, sector);
  }

  @put('/api/sectores/{id}', {
    responses: {
      '204': {
        description: 'Sector PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() sector: Sector,
  ): Promise<void> {
    await this.sectorRepository.replaceById(id, sector);
  }

  @del('/api/sectores/{id}', {
    responses: {
      '204': {
        description: 'Sector DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.sectorRepository.deleteById(id);
  }
}
