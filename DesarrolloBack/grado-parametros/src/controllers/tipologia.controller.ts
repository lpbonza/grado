import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Tipologia } from '../models';
import { TipologiaRepository } from '../repositories';

export class TipologiaController {
  constructor(
    @repository(TipologiaRepository)
    public tipologiaRepository: TipologiaRepository,
  ) { }

  @post('/api/tipologias', {
    responses: {
      '200': {
        description: 'Tipologia model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Tipologia) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tipologia, { exclude: ['idTipologia'] }),
        },
      },
    })
    tipologia: Omit<Tipologia, 'idTipologia'>,
  ): Promise<Tipologia> {
    return this.tipologiaRepository.create(tipologia);
  }

  @get('/api/tipologias/count', {
    responses: {
      '200': {
        description: 'Tipologia model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Tipologia)) where?: Where<Tipologia>,
  ): Promise<Count> {
    return this.tipologiaRepository.count(where);
  }

  @get('/api/tipologias', {
    responses: {
      '200': {
        description: 'Array of Tipologia model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Tipologia) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Tipologia)) filter?: Filter<Tipologia>,
  ): Promise<Tipologia[]> {
    return this.tipologiaRepository.find(filter);
  }

  @patch('/api/tipologias', {
    responses: {
      '200': {
        description: 'Tipologia PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tipologia, { partial: true }),
        },
      },
    })
    tipologia: Tipologia,
    @param.query.object('where', getWhereSchemaFor(Tipologia)) where?: Where<Tipologia>,
  ): Promise<Count> {
    return this.tipologiaRepository.updateAll(tipologia, where);
  }

  @get('/api/tipologias/{id}', {
    responses: {
      '200': {
        description: 'Tipologia model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Tipologia) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<Tipologia> {
    return this.tipologiaRepository.findById(id);
  }

  @patch('/api/tipologias/{id}', {
    responses: {
      '204': {
        description: 'Tipologia PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Tipologia, { partial: true }),
        },
      },
    })
    tipologia: Tipologia,
  ): Promise<void> {
    await this.tipologiaRepository.updateById(id, tipologia);
  }

  @put('/api/tipologias/{id}', {
    responses: {
      '204': {
        description: 'Tipologia PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() tipologia: Tipologia,
  ): Promise<void> {
    await this.tipologiaRepository.replaceById(id, tipologia);
  }

  @del('/api/tipologias/{id}', {
    responses: {
      '204': {
        description: 'Tipologia DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.tipologiaRepository.deleteById(id);
  }
}
