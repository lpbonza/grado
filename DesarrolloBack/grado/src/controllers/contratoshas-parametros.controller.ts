import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { ContratosHasParametros } from '../models';
import { ContratosHasParametrosRepository } from '../repositories';

export class ContratoshasParametrosController {
  constructor(
    @repository(ContratosHasParametrosRepository)
    public contratosHasParametrosRepository: ContratosHasParametrosRepository,
  ) { }

  @post('/contratospersonas', {
    responses: {
      '200': {
        description: 'ContratosHasParametros model instance',
        content: { 'application/json': { schema: getModelSchemaRef(ContratosHasParametros) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContratosHasParametros),
        },
      },
    })
    contratosHasParametros: Omit<ContratosHasParametros, 'Contratos_idContratos'>,
  ): Promise<ContratosHasParametros> {
    return this.contratosHasParametrosRepository.create(contratosHasParametros);
  }

  @get('/contratospersonas/count', {
    responses: {
      '200': {
        description: 'ContratosHasParametros model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(ContratosHasParametros)) where?: Where<ContratosHasParametros>,
  ): Promise<Count> {
    return this.contratosHasParametrosRepository.count(where);
  }

  @get('/contratospersonas', {
    responses: {
      '200': {
        description: 'Array of ContratosHasParametros model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(ContratosHasParametros) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(ContratosHasParametros)) filter?: Filter<ContratosHasParametros>,
  ): Promise<ContratosHasParametros[]> {
    return this.contratosHasParametrosRepository.find(filter);
  }

  @patch('/contratospersonas', {
    responses: {
      '200': {
        description: 'ContratosHasParametros PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContratosHasParametros, { partial: true }),
        },
      },
    })
    contratosHasParametros: ContratosHasParametros,
    @param.query.object('where', getWhereSchemaFor(ContratosHasParametros)) where?: Where<ContratosHasParametros>,
  ): Promise<Count> {
    return this.contratosHasParametrosRepository.updateAll(contratosHasParametros, where);
  }

  @get('/contratospersonas/{id}', {
    responses: {
      '200': {
        description: 'ContratosHasParametros model instance',
        content: { 'application/json': { schema: getModelSchemaRef(ContratosHasParametros) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<ContratosHasParametros> {
    return this.contratosHasParametrosRepository.findById(id);
  }

  @patch('/contratospersonas/{id}', {
    responses: {
      '204': {
        description: 'ContratosHasParametros PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ContratosHasParametros, { partial: true }),
        },
      },
    })
    contratosHasParametros: ContratosHasParametros,
  ): Promise<void> {
    await this.contratosHasParametrosRepository.updateById(id, contratosHasParametros);
  }

  @put('/contratospersonas/{id}', {
    responses: {
      '204': {
        description: 'ContratosHasParametros PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() contratosHasParametros: ContratosHasParametros,
  ): Promise<void> {
    await this.contratosHasParametrosRepository.replaceById(id, contratosHasParametros);
  }

  @del('/contratospersonas/{id}', {
    responses: {
      '204': {
        description: 'ContratosHasParametros DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.contratosHasParametrosRepository.deleteById(id);
  }
}
