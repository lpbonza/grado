import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import { FirmaContrato } from '../models';
import { FirmaContratoRepository } from '../repositories';

export class FirmaContratoController {
  constructor(
    @repository(FirmaContratoRepository)
    public firmaContratoRepository: FirmaContratoRepository,
  ) { }

  @post('/firmacontratos', {
    responses: {
      '200': {
        description: 'FirmaContrato model instance',
        content: { 'application/json': { schema: getModelSchemaRef(FirmaContrato) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FirmaContrato),
        },
      },
    })
    firmaContrato: Omit<FirmaContrato, 'Documento'>,
  ): Promise<FirmaContrato> {
    return this.firmaContratoRepository.create(firmaContrato);
  }

  @get('/firmacontratos/count', {
    responses: {
      '200': {
        description: 'FirmaContrato model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(FirmaContrato)) where?: Where<FirmaContrato>,
  ): Promise<Count> {
    return this.firmaContratoRepository.count(where);
  }

  @get('/firmacontratos', {
    responses: {
      '200': {
        description: 'Array of FirmaContrato model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(FirmaContrato) },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(FirmaContrato)) filter?: Filter<FirmaContrato>,
  ): Promise<FirmaContrato[]> {
    return this.firmaContratoRepository.find(filter);
  }

  @patch('/firmacontratos', {
    responses: {
      '200': {
        description: 'FirmaContrato PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FirmaContrato, { partial: true }),
        },
      },
    })
    firmaContrato: FirmaContrato,
    @param.query.object('where', getWhereSchemaFor(FirmaContrato)) where?: Where<FirmaContrato>,
  ): Promise<Count> {
    return this.firmaContratoRepository.updateAll(firmaContrato, where);
  }

  @get('/firmacontratos/{id}', {
    responses: {
      '200': {
        description: 'FirmaContrato model instance',
        content: { 'application/json': { schema: getModelSchemaRef(FirmaContrato) } },
      },
    },
  })
  async findById(@param.path.number('id') id: number): Promise<FirmaContrato> {
    return this.firmaContratoRepository.findById(id);
  }


  @get('/api/firmacontratos/{id}', {
    responses: {
      '200': {
        description: 'FirmaContrato model instance',
        content: { 'application/json': { schema: getModelSchemaRef(FirmaContrato) } },
      },
    },
  })
  async findByIdFirmante(@param.path.number('id') id: number): Promise<object> {
    const fimante = await this.firmaContratoRepository.findOne({ 'where': { 'Empresa_nitEmpresa': id } });
    console.info(fimante);
    if (!fimante) {
      console.warn(fimante);
      console.error('No se encontraron registros asociados');
      throw new HttpErrors.NotFound('No se encuentra registrados')
    }
    console.info('La consulta trae datos');
    return fimante;

  }

  @patch('/firmacontratos/{id}', {
    responses: {
      '204': {
        description: 'FirmaContrato PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(FirmaContrato, { partial: true }),
        },
      },
    })
    firmaContrato: FirmaContrato,
  ): Promise<void> {
    await this.firmaContratoRepository.updateById(id, firmaContrato);
  }

  @put('/firmacontratos/{id}', {
    responses: {
      '204': {
        description: 'FirmaContrato PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() firmaContrato: FirmaContrato,
  ): Promise<void> {
    await this.firmaContratoRepository.replaceById(id, firmaContrato);
  }

  @del('/firmacontratos/{id}', {
    responses: {
      '204': {
        description: 'FirmaContrato DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.firmaContratoRepository.deleteById(id);
  }
}
