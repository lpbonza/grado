import {DefaultCrudRepository} from '@loopback/repository';
import {Contacto, ContactoRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ContactoRepository extends DefaultCrudRepository<
  Contacto,
  typeof Contacto.prototype.IdContacto,
  ContactoRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(Contacto, dataSource);
  }
}
