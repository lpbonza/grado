import {DefaultCrudRepository} from '@loopback/repository';
import {FirmaContrato, FirmaContratoRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FirmaContratoRepository extends DefaultCrudRepository<
  FirmaContrato,
  typeof FirmaContrato.prototype.Documento,
  FirmaContratoRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(FirmaContrato, dataSource);
  }
}
