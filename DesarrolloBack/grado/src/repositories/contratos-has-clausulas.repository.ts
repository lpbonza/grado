import {DefaultCrudRepository} from '@loopback/repository';
import {ContratosHasClausulas, ContratosHasClausulasRelations} from '../models';
import {MyDataMysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ContratosHasClausulasRepository extends DefaultCrudRepository<
  ContratosHasClausulas,
  typeof ContratosHasClausulas.prototype.Contratos_idContratos,
  ContratosHasClausulasRelations
> {
  constructor(
    @inject('datasources.MyDataMysql') dataSource: MyDataMysqlDataSource,
  ) {
    super(ContratosHasClausulas, dataSource);
  }
}
