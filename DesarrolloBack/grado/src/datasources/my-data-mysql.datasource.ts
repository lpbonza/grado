import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './my-data-mysql.datasource.json';

export class MyDataMysqlDataSource extends juggler.DataSource {
  static dataSourceName = 'MyDataMysql';

  constructor(
    @inject('datasources.config.MyDataMysql', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
