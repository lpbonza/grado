import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Empresa extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  nitEmpresa: number;

  @property({
    type: 'string',
    required: true,
  })
  Nombre: string;

  @property({
    type: 'number',
    required: true,
  })
  numeroEmpleados: number;

  @property({
    type: 'string',
    required: true,
  })
  Direccion: string;

  @property({
    type: 'string',
    required: true,
  })
  Pagina: string;

  @property({
    type: 'number',
    required: true,
  })
  Sector_idSector: number;

  @property({
    type: 'number',
    required: true,
  })
  Tipologia_idTipologia: number;

  @property({
    type: 'number',
  })
  Pais_idPais?: number;

  @property({
    type: 'number',
    required: true,
  })
  Departamento_idDepartamento: number;

  @property({
    type: 'number',
    required: true,
  })
  Ciudad_idCiudad: number;


  constructor(data?: Partial<Empresa>) {
    super(data);
  }
}

export interface EmpresaRelations {
  // describe navigational properties here
}

export type EmpresaWithRelations = Empresa & EmpresaRelations;
