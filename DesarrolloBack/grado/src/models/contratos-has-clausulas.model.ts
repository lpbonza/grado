import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class ContratosHasClausulas extends Entity {
  @property({
    type: 'number',
    id: true,
    required: true,
  })
  Contratos_idContratos: number;

  @property({
    type: 'number',
    required: true,
  })
  Clausulas_idClausulas: number;


  constructor(data?: Partial<ContratosHasClausulas>) {
    super(data);
  }
}

export interface ContratosHasClausulasRelations {
  // describe navigational properties here
}

export type ContratosHasClausulasWithRelations = ContratosHasClausulas & ContratosHasClausulasRelations;
