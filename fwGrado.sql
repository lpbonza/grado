-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-10-2019 a las 22:01:22
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fwGrado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Agreements`
--

CREATE TABLE `Agreements` (
  `idAgreements` int(11) NOT NULL,
  `Agreement` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Permissions`
--

CREATE TABLE `Permissions` (
  `idPermissions` int(11) NOT NULL,
  `Permissions` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Roles`
--

CREATE TABLE `Roles` (
  `idRoles` int(11) NOT NULL,
  `Rol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Roles`
--

INSERT INTO `Roles` (`idRoles`, `Rol`) VALUES
(2, 'Administrador'),
(3, 'Empresa'),
(4, 'Persona');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Roles_has_Permissions`
--

CREATE TABLE `Roles_has_Permissions` (
  `Roles_idRoles` int(11) NOT NULL,
  `Permissions_idPermissions` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoDocumento`
--

CREATE TABLE `TipoDocumento` (
  `idTipoDocumento` int(11) NOT NULL,
  `TipoDocumento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `TipoDocumento`
--

INSERT INTO `TipoDocumento` (`idTipoDocumento`, `TipoDocumento`) VALUES
(1, 'Cedula de ciudadania'),
(2, 'Cedula de extranjeria'),
(3, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Users`
--

CREATE TABLE `Users` (
  `idUsers` int(11) NOT NULL,
  `Email` longtext NOT NULL,
  `Name` longtext NOT NULL,
  `Password` longtext NOT NULL,
  `Celular` varchar(45) DEFAULT NULL,
  `Roles_idRoles` int(11) NOT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Users`
--

INSERT INTO `Users` (`idUsers`, `Email`, `Name`, `Password`, `Celular`, `Roles_idRoles`, `TipoDocumento_idTipoDocumento`) VALUES
(16, 'ldannynavas@gmail.com', 'Luis Daniel Gordo Navas', 'ea03f2a15188371b7783126218ef96871b95685215f6d32c4777c4b634d1248b', '3163418533', 2, 0),
(1019042184, 'dannielnavas@gmail.com', 'Luis Daniel Gordo Navas', 'ea03f2a15188371b7783126218ef96871b95685215f6d32c4777c4b634d1248b', '3163418533', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Agreements`
--
ALTER TABLE `Agreements`
  ADD PRIMARY KEY (`idAgreements`);

--
-- Indices de la tabla `Permissions`
--
ALTER TABLE `Permissions`
  ADD PRIMARY KEY (`idPermissions`);

--
-- Indices de la tabla `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`idRoles`);

--
-- Indices de la tabla `Roles_has_Permissions`
--
ALTER TABLE `Roles_has_Permissions`
  ADD PRIMARY KEY (`Roles_idRoles`,`Permissions_idPermissions`),
  ADD KEY `fk_Roles_has_Permissions_Permissions1_idx` (`Permissions_idPermissions`),
  ADD KEY `fk_Roles_has_Permissions_Roles_idx` (`Roles_idRoles`);

--
-- Indices de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  ADD PRIMARY KEY (`idTipoDocumento`);

--
-- Indices de la tabla `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`idUsers`),
  ADD KEY `fk_Users_Roles1_idx` (`Roles_idRoles`),
  ADD KEY `fk_Users_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Agreements`
--
ALTER TABLE `Agreements`
  MODIFY `idAgreements` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Permissions`
--
ALTER TABLE `Permissions`
  MODIFY `idPermissions` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Roles`
--
ALTER TABLE `Roles`
  MODIFY `idRoles` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  MODIFY `idTipoDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Users`
--
ALTER TABLE `Users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Roles_has_Permissions`
--
ALTER TABLE `Roles_has_Permissions`
  ADD CONSTRAINT `fk_Roles_has_Permissions_Permissions1` FOREIGN KEY (`Permissions_idPermissions`) REFERENCES `Permissions` (`idPermissions`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Roles_has_Permissions_Roles` FOREIGN KEY (`Roles_idRoles`) REFERENCES `Roles` (`idRoles`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Users`
--
ALTER TABLE `Users`
  ADD CONSTRAINT `fk_Users_Roles1` FOREIGN KEY (`Roles_idRoles`) REFERENCES `Roles` (`idRoles`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Users_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
