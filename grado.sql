-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-10-2019 a las 22:01:05
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `grado`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AlmacenContratos`
--

CREATE TABLE `AlmacenContratos` (
  `idAlmacenContratos` int(11) NOT NULL,
  `AlmacenContratos` longtext,
  `TipoContrato_idTipoContrato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cargo`
--

CREATE TABLE `Cargo` (
  `idCargo` int(11) NOT NULL,
  `Cargo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Ciudad`
--

CREATE TABLE `Ciudad` (
  `idCiudad` int(11) NOT NULL,
  `Ciudad` varchar(45) DEFAULT NULL,
  `Departamento_idDepartamento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Ciudad`
--

INSERT INTO `Ciudad` (`idCiudad`, `Ciudad`, `Departamento_idDepartamento`) VALUES
(1, 'MEDELLIN', 1),
(2, 'ABEJORRAL', 1),
(3, 'ABRIAQUI', 1),
(4, 'ALEJANDRIA', 1),
(5, 'AMAGA', 1),
(6, 'AMALFI', 1),
(7, 'ANDES', 1),
(8, 'ANGELOPOLIS', 1),
(9, 'ANGOSTURA', 1),
(10, 'ANORI', 1),
(11, 'SANTAFE DE ANTIOQUIA', 1),
(12, 'ANZA', 1),
(13, 'APARTADO', 1),
(14, 'ARBOLETES', 1),
(15, 'ARGELIA', 1),
(16, 'ARMENIA', 1),
(17, 'BARBOSA', 1),
(18, 'BELMIRA', 1),
(19, 'BELLO', 1),
(20, 'BETANIA', 1),
(21, 'BETULIA', 1),
(22, 'CIUDAD BOLIVAR', 1),
(23, 'BRICEÑO', 1),
(24, 'BURITICA', 1),
(25, 'CACERES', 1),
(26, 'CAICEDO', 1),
(27, 'CALDAS', 1),
(28, 'CAMPAMENTO', 1),
(29, 'CAÑASGORDAS', 1),
(30, 'CARACOLI', 1),
(31, 'CARAMANTA', 1),
(32, 'CAREPA', 1),
(33, 'EL CARMEN DE VIBORAL', 1),
(34, 'CAROLINA', 1),
(35, 'CAUCASIA', 1),
(36, 'CHIGORODO', 1),
(37, 'CISNEROS', 1),
(38, 'COCORNA', 1),
(39, 'CONCEPCION', 1),
(40, 'CONCORDIA', 1),
(41, 'COPACABANA', 1),
(42, 'DABEIBA', 1),
(43, 'DON MATIAS', 1),
(44, 'EBEJICO', 1),
(45, 'EL BAGRE', 1),
(46, 'ENTRERRIOS', 1),
(47, 'ENVIGADO', 1),
(48, 'FREDONIA', 1),
(49, 'FRONTINO', 1),
(50, 'GIRALDO', 1),
(51, 'GIRARDOTA', 1),
(52, 'GOMEZ PLATA', 1),
(53, 'GRANADA', 1),
(54, 'GUADALUPE', 1),
(55, 'GUARNE', 1),
(56, 'GUATAPE', 1),
(57, 'HELICONIA', 1),
(58, 'HISPANIA', 1),
(59, 'ITAGUI', 1),
(60, 'ITUANGO', 1),
(61, 'JARDIN', 1),
(62, 'JERICO', 1),
(63, 'LA CEJA', 1),
(64, 'LA ESTRELLA', 1),
(65, 'LA PINTADA', 1),
(66, 'LA UNION', 1),
(67, 'LIBORINA', 1),
(68, 'MACEO', 1),
(69, 'MARINILLA', 1),
(70, 'MONTEBELLO', 1),
(71, 'MURINDO', 1),
(72, 'MUTATA', 1),
(73, 'NARIÑO', 1),
(74, 'NECOCLI', 1),
(75, 'NECHI', 1),
(76, 'OLAYA', 1),
(77, 'PEÐOL', 1),
(78, 'PEQUE', 1),
(79, 'PUEBLORRICO', 1),
(80, 'PUERTO BERRIO', 1),
(81, 'PUERTO NARE', 1),
(82, 'PUERTO TRIUNFO', 1),
(83, 'REMEDIOS', 1),
(84, 'RETIRO', 1),
(85, 'RIONEGRO', 1),
(86, 'SABANALARGA', 1),
(87, 'SABANETA', 1),
(88, 'SALGAR', 1),
(89, 'SAN ANDRES DE CUERQUIA', 1),
(90, 'SAN CARLOS', 1),
(91, 'SAN FRANCISCO', 1),
(92, 'SAN JERONIMO', 1),
(93, 'SAN JOSE DE LA MONTAÑA', 1),
(94, 'SAN JUAN DE URABA', 1),
(95, 'SAN LUIS', 1),
(96, 'SAN PEDRO', 1),
(97, 'SAN PEDRO DE URABA', 1),
(98, 'SAN RAFAEL', 1),
(99, 'SAN ROQUE', 1),
(100, 'SAN VICENTE', 1),
(101, 'SANTA BARBARA', 1),
(102, 'SANTA ROSA DE OSOS', 1),
(103, 'SANTO DOMINGO', 1),
(104, 'EL SANTUARIO', 1),
(105, 'SEGOVIA', 1),
(106, 'SONSON', 1),
(107, 'SOPETRAN', 1),
(108, 'TAMESIS', 1),
(109, 'TARAZA', 1),
(110, 'TARSO', 1),
(111, 'TITIRIBI', 1),
(112, 'TOLEDO', 1),
(113, 'TURBO', 1),
(114, 'URAMITA', 1),
(115, 'URRAO', 1),
(116, 'VALDIVIA', 1),
(117, 'VALPARAISO', 1),
(118, 'VEGACHI', 1),
(119, 'VENECIA', 1),
(120, 'VIGIA DEL FUERTE', 1),
(121, 'YALI', 1),
(122, 'YARUMAL', 1),
(123, 'YOLOMBO', 1),
(124, 'YONDO', 1),
(125, 'ZARAGOZA', 1),
(126, 'BARRANQUILLA', 3),
(127, 'BARANOA', 3),
(128, 'CAMPO DE LA CRUZ', 3),
(129, 'CANDELARIA', 3),
(130, 'GALAPA', 3),
(131, 'JUAN DE ACOSTA', 3),
(132, 'LURUACO', 3),
(133, 'MALAMBO', 3),
(134, 'MANATI', 3),
(135, 'PALMAR DE VARELA', 3),
(136, 'PIOJO', 3),
(137, 'POLONUEVO', 3),
(138, 'PONEDERA', 3),
(139, 'PUERTO COLOMBIA', 3),
(140, 'REPELON', 3),
(141, 'SABANAGRANDE', 3),
(142, 'SABANALARGA', 3),
(143, 'SANTA LUCIA', 3),
(144, 'SANTO TOMAS', 3),
(145, 'SOLEDAD', 3),
(146, 'SUAN', 3),
(147, 'TUBARA', 3),
(148, 'USIACURI', 3),
(149, 'BOGOTA, D.C.', 5),
(150, 'CARTAGENA', 7),
(151, 'ACHI', 7),
(152, 'ALTOS DEL ROSARIO', 7),
(153, 'ARENAL', 7),
(154, 'ARJONA', 7),
(155, 'ARROYOHONDO', 7),
(156, 'BARRANCO DE LOBA', 7),
(157, 'CALAMAR', 7),
(158, 'CANTAGALLO', 7),
(159, 'CICUCO', 7),
(160, 'CORDOBA', 7),
(161, 'CLEMENCIA', 7),
(162, 'EL CARMEN DE BOLIVAR', 7),
(163, 'EL GUAMO', 7),
(164, 'EL PEÑON', 7),
(165, 'HATILLO DE LOBA', 7),
(166, 'MAGANGUE', 7),
(167, 'MAHATES', 7),
(168, 'MARGARITA', 7),
(169, 'MARIA LA BAJA', 7),
(170, 'MONTECRISTO', 7),
(171, 'MOMPOS', 7),
(172, 'NOROSI', 7),
(173, 'MORALES', 7),
(174, 'PINILLOS', 7),
(175, 'REGIDOR', 7),
(176, 'RIO VIEJO', 7),
(177, 'SAN CRISTOBAL', 7),
(178, 'SAN ESTANISLAO', 7),
(179, 'SAN FERNANDO', 7),
(180, 'SAN JACINTO', 7),
(181, 'SAN JACINTO DEL CAUCA', 7),
(182, 'SAN JUAN NEPOMUCENO', 7),
(183, 'SAN MARTIN DE LOBA', 7),
(184, 'SAN PABLO', 7),
(185, 'SANTA CATALINA', 7),
(186, 'SANTA ROSA', 7),
(187, 'SANTA ROSA DEL SUR', 7),
(188, 'SIMITI', 7),
(189, 'SOPLAVIENTO', 7),
(190, 'TALAIGUA NUEVO', 7),
(191, 'TIQUISIO', 7),
(192, 'TURBACO', 7),
(193, 'TURBANA', 7),
(194, 'VILLANUEVA', 7),
(195, 'ZAMBRANO', 7),
(196, 'TUNJA', 9),
(197, 'ALMEIDA', 9),
(198, 'AQUITANIA', 9),
(199, 'ARCABUCO', 9),
(200, 'BELEN', 9),
(201, 'BERBEO', 9),
(202, 'BETEITIVA', 9),
(203, 'BOAVITA', 9),
(204, 'BOYACA', 9),
(205, 'BRICEÑO', 9),
(206, 'BUENAVISTA', 9),
(207, 'BUSBANZA', 9),
(208, 'CALDAS', 9),
(209, 'CAMPOHERMOSO', 9),
(210, 'CERINZA', 9),
(211, 'CHINAVITA', 9),
(212, 'CHIQUINQUIRA', 9),
(213, 'CHISCAS', 9),
(214, 'CHITA', 9),
(215, 'CHITARAQUE', 9),
(216, 'CHIVATA', 9),
(217, 'CIENEGA', 9),
(218, 'COMBITA', 9),
(219, 'COPER', 9),
(220, 'CORRALES', 9),
(221, 'COVARACHIA', 9),
(222, 'CUBARA', 9),
(223, 'CUCAITA', 9),
(224, 'CUITIVA', 9),
(225, 'CHIQUIZA', 9),
(226, 'CHIVOR', 9),
(227, 'DUITAMA', 9),
(228, 'EL COCUY', 9),
(229, 'EL ESPINO', 9),
(230, 'FIRAVITOBA', 9),
(231, 'FLORESTA', 9),
(232, 'GACHANTIVA', 9),
(233, 'GAMEZA', 9),
(234, 'GARAGOA', 9),
(235, 'GUACAMAYAS', 9),
(236, 'GUATEQUE', 9),
(237, 'GUAYATA', 9),
(238, 'GsICAN', 9),
(239, 'IZA', 9),
(240, 'JENESANO', 9),
(241, 'JERICO', 9),
(242, 'LABRANZAGRANDE', 9),
(243, 'LA CAPILLA', 9),
(244, 'LA VICTORIA', 9),
(245, 'LA UVITA', 9),
(246, 'VILLA DE LEYVA', 9),
(247, 'MACANAL', 9),
(248, 'MARIPI', 9),
(249, 'MIRAFLORES', 9),
(250, 'MONGUA', 9),
(251, 'MONGUI', 9),
(252, 'MONIQUIRA', 9),
(253, 'MOTAVITA', 9),
(254, 'MUZO', 9),
(255, 'NOBSA', 9),
(256, 'NUEVO COLON', 9),
(257, 'OICATA', 9),
(258, 'OTANCHE', 9),
(259, 'PACHAVITA', 9),
(260, 'PAEZ', 9),
(261, 'PAIPA', 9),
(262, 'PAJARITO', 9),
(263, 'PANQUEBA', 9),
(264, 'PAUNA', 9),
(265, 'PAYA', 9),
(266, 'PAZ DE RIO', 9),
(267, 'PESCA', 9),
(268, 'PISBA', 9),
(269, 'PUERTO BOYACA', 9),
(270, 'QUIPAMA', 9),
(271, 'RAMIRIQUI', 9),
(272, 'RAQUIRA', 9),
(273, 'RONDON', 9),
(274, 'SABOYA', 9),
(275, 'SACHICA', 9),
(276, 'SAMACA', 9),
(277, 'SAN EDUARDO', 9),
(278, 'SAN JOSE DE PARE', 9),
(279, 'SAN LUIS DE GACENO', 9),
(280, 'SAN MATEO', 9),
(281, 'SAN MIGUEL DE SEMA', 9),
(282, 'SAN PABLO DE BORBUR', 9),
(283, 'SANTANA', 9),
(284, 'SANTA MARIA', 9),
(285, 'SANTA ROSA DE VITERBO', 9),
(286, 'SANTA SOFIA', 9),
(287, 'SATIVANORTE', 9),
(288, 'SATIVASUR', 9),
(289, 'SIACHOQUE', 9),
(290, 'SOATA', 9),
(291, 'SOCOTA', 9),
(292, 'SOCHA', 9),
(293, 'SOGAMOSO', 9),
(294, 'SOMONDOCO', 9),
(295, 'SORA', 9),
(296, 'SOTAQUIRA', 9),
(297, 'SORACA', 9),
(298, 'SUSACON', 9),
(299, 'SUTAMARCHAN', 9),
(300, 'SUTATENZA', 9),
(301, 'TASCO', 9),
(302, 'TENZA', 9),
(303, 'TIBANA', 9),
(304, 'TIBASOSA', 9),
(305, 'TINJACA', 9),
(306, 'TIPACOQUE', 9),
(307, 'TOCA', 9),
(308, 'TOGsI', 9),
(309, 'TOPAGA', 9),
(310, 'TOTA', 9),
(311, 'TUNUNGUA', 9),
(312, 'TURMEQUE', 9),
(313, 'TUTA', 9),
(314, 'TUTAZA', 9),
(315, 'UMBITA', 9),
(316, 'VENTAQUEMADA', 9),
(317, 'VIRACACHA', 9),
(318, 'ZETAQUIRA', 9),
(319, 'MANIZALES', 11),
(320, 'AGUADAS', 11),
(321, 'ANSERMA', 11),
(322, 'ARANZAZU', 11),
(323, 'BELALCAZAR', 11),
(324, 'CHINCHINA', 11),
(325, 'FILADELFIA', 11),
(326, 'LA DORADA', 11),
(327, 'LA MERCED', 11),
(328, 'MANZANARES', 11),
(329, 'MARMATO', 11),
(330, 'MARQUETALIA', 11),
(331, 'MARULANDA', 11),
(332, 'NEIRA', 11),
(333, 'NORCASIA', 11),
(334, 'PACORA', 11),
(335, 'PALESTINA', 11),
(336, 'PENSILVANIA', 11),
(337, 'RIOSUCIO', 11),
(338, 'RISARALDA', 11),
(339, 'SALAMINA', 11),
(340, 'SAMANA', 11),
(341, 'SAN JOSE', 11),
(342, 'SUPIA', 11),
(343, 'VICTORIA', 11),
(344, 'VILLAMARIA', 11),
(345, 'VITERBO', 11),
(346, 'FLORENCIA', 13),
(347, 'ALBANIA', 13),
(348, 'BELEN DE LOS ANDAQUIES', 13),
(349, 'CARTAGENA DEL CHAIRA', 13),
(350, 'CURILLO', 13),
(351, 'EL DONCELLO', 13),
(352, 'EL PAUJIL', 13),
(353, 'LA MONTAÑITA', 13),
(354, 'MILAN', 13),
(355, 'MORELIA', 13),
(356, 'PUERTO RICO', 13),
(357, 'SAN JOSE DEL FRAGUA', 13),
(358, 'SAN VICENTE DEL CAGUAN', 13),
(359, 'SOLANO', 13),
(360, 'SOLITA', 13),
(361, 'VALPARAISO', 13),
(362, 'POPAYAN', 15),
(363, 'ALMAGUER', 15),
(364, 'ARGELIA', 15),
(365, 'BALBOA', 15),
(366, 'BOLIVAR', 15),
(367, 'BUENOS AIRES', 15),
(368, 'CAJIBIO', 15),
(369, 'CALDONO', 15),
(370, 'CALOTO', 15),
(371, 'CORINTO', 15),
(372, 'EL TAMBO', 15),
(373, 'FLORENCIA', 15),
(374, 'GUACHENE', 15),
(375, 'GUAPI', 15),
(376, 'INZA', 15),
(377, 'JAMBALO', 15),
(378, 'LA SIERRA', 15),
(379, 'LA VEGA', 15),
(380, 'LOPEZ', 15),
(381, 'MERCADERES', 15),
(382, 'MIRANDA', 15),
(383, 'MORALES', 15),
(384, 'PADILLA', 15),
(385, 'PAEZ', 15),
(386, 'PATIA', 15),
(387, 'PIAMONTE', 15),
(388, 'PIENDAMO', 15),
(389, 'PUERTO TEJADA', 15),
(390, 'PURACE', 15),
(391, 'ROSAS', 15),
(392, 'SAN SEBASTIAN', 15),
(393, 'SANTANDER DE QUILICHAO', 15),
(394, 'SANTA ROSA', 15),
(395, 'SILVIA', 15),
(396, 'SOTARA', 15),
(397, 'SUAREZ', 15),
(398, 'SUCRE', 15),
(399, 'TIMBIO', 15),
(400, 'TIMBIQUI', 15),
(401, 'TORIBIO', 15),
(402, 'TOTORO', 15),
(403, 'VILLA RICA', 15),
(404, 'VALLEDUPAR', 17),
(405, 'AGUACHICA', 17),
(406, 'AGUSTIN CODAZZI', 17),
(407, 'ASTREA', 17),
(408, 'BECERRIL', 17),
(409, 'BOSCONIA', 17),
(410, 'CHIMICHAGUA', 17),
(411, 'CHIRIGUANA', 17),
(412, 'CURUMANI', 17),
(413, 'EL COPEY', 17),
(414, 'EL PASO', 17),
(415, 'GAMARRA', 17),
(416, 'GONZALEZ', 17),
(417, 'LA GLORIA', 17),
(418, 'LA JAGUA DE IBIRICO', 17),
(419, 'MANAURE', 17),
(420, 'PAILITAS', 17),
(421, 'PELAYA', 17),
(422, 'PUEBLO BELLO', 17),
(423, 'RIO DE ORO', 17),
(424, 'LA PAZ', 17),
(425, 'SAN ALBERTO', 17),
(426, 'SAN DIEGO', 17),
(427, 'SAN MARTIN', 17),
(428, 'TAMALAMEQUE', 17),
(429, 'MONTERIA', 19),
(430, 'AYAPEL', 19),
(431, 'BUENAVISTA', 19),
(432, 'CANALETE', 19),
(433, 'CERETE', 19),
(434, 'CHIMA', 19),
(435, 'CHINU', 19),
(436, 'CIENAGA DE ORO', 19),
(437, 'COTORRA', 19),
(438, 'LA APARTADA', 19),
(439, 'LORICA', 19),
(440, 'LOS CORDOBAS', 19),
(441, 'MOMIL', 19),
(442, 'MONTELIBANO', 19),
(443, 'MOÑITOS', 19),
(444, 'PLANETA RICA', 19),
(445, 'PUEBLO NUEVO', 19),
(446, 'PUERTO ESCONDIDO', 19),
(447, 'PUERTO LIBERTADOR', 19),
(448, 'PURISIMA', 19),
(449, 'SAHAGUN', 19),
(450, 'SAN ANDRES SOTAVENTO', 19),
(451, 'SAN ANTERO', 19),
(452, 'SAN BERNARDO DEL VIENTO', 19),
(453, 'SAN CARLOS', 19),
(454, 'SAN PELAYO', 19),
(455, 'TIERRALTA', 19),
(456, 'VALENCIA', 19),
(457, 'AGUA DE DIOS', 21),
(458, 'ALBAN', 21),
(459, 'ANAPOIMA', 21),
(460, 'ANOLAIMA', 21),
(461, 'ARBELAEZ', 21),
(462, 'BELTRAN', 21),
(463, 'BITUIMA', 21),
(464, 'BOJACA', 21),
(465, 'CABRERA', 21),
(466, 'CACHIPAY', 21),
(467, 'CAJICA', 21),
(468, 'CAPARRAPI', 21),
(469, 'CAQUEZA', 21),
(470, 'CARMEN DE CARUPA', 21),
(471, 'CHAGUANI', 21),
(472, 'CHIA', 21),
(473, 'CHIPAQUE', 21),
(474, 'CHOACHI', 21),
(475, 'CHOCONTA', 21),
(476, 'COGUA', 21),
(477, 'COTA', 21),
(478, 'CUCUNUBA', 21),
(479, 'EL COLEGIO', 21),
(480, 'EL PEÑON', 21),
(481, 'EL ROSAL', 21),
(482, 'FACATATIVA', 21),
(483, 'FOMEQUE', 21),
(484, 'FOSCA', 21),
(485, 'FUNZA', 21),
(486, 'FUQUENE', 21),
(487, 'FUSAGASUGA', 21),
(488, 'GACHALA', 21),
(489, 'GACHANCIPA', 21),
(490, 'GACHETA', 21),
(491, 'GAMA', 21),
(492, 'GIRARDOT', 21),
(493, 'GRANADA', 21),
(494, 'GUACHETA', 21),
(495, 'GUADUAS', 21),
(496, 'GUASCA', 21),
(497, 'GUATAQUI', 21),
(498, 'GUATAVITA', 21),
(499, 'GUAYABAL DE SIQUIMA', 21),
(500, 'GUAYABETAL', 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Clausulas`
--

CREATE TABLE `Clausulas` (
  `idClausulas` int(11) NOT NULL,
  `Clausulas` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contacto`
--

CREATE TABLE `Contacto` (
  `IdContacto` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL,
  `Cargo_idCargo` int(11) NOT NULL,
  `Empresa_nitEmpresa` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contratos`
--

CREATE TABLE `Contratos` (
  `idContratos` bigint(20) NOT NULL,
  `FechaInicio` date NOT NULL,
  `FechaFinal` date NOT NULL,
  `FechaContrato` date DEFAULT NULL,
  `TipoContrato_idTipoContrato` int(11) NOT NULL,
  `FirmaContrato_Documento` bigint(20) NOT NULL,
  `Empresa_nitEmpresa` bigint(20) NOT NULL,
  `Persona_idPersona` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ContratosHasClausulas`
--

CREATE TABLE `ContratosHasClausulas` (
  `Contratos_idContratos` bigint(20) NOT NULL,
  `Clausulas_idClausulas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ContratosHasParametros`
--

CREATE TABLE `ContratosHasParametros` (
  `Contratos_idContratos` bigint(20) NOT NULL,
  `Parametros_idParametros` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Departamento`
--

CREATE TABLE `Departamento` (
  `idDepartamento` int(11) NOT NULL,
  `Departamento` varchar(45) DEFAULT NULL,
  `Pais_idPais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Departamento`
--

INSERT INTO `Departamento` (`idDepartamento`, `Departamento`, `Pais_idPais`) VALUES
(1, 'ANTIOQUIA', 169),
(3, 'ATLANTICO', 169),
(5, 'BOGOTA', 169),
(7, 'BOLIVAR', 169),
(9, 'BOYACA', 169),
(11, 'CALDAS', 169),
(13, 'CAQUETA', 169),
(15, 'CAUCA', 169),
(17, 'CESAR', 169),
(19, 'CORDOBA', 169),
(21, 'CUNDINAMARCA', 169),
(23, 'CHOCO', 169),
(25, 'HUILA', 169),
(27, 'LA GUAJIRA', 169),
(29, 'MAGDALENA', 169),
(31, 'META', 169),
(33, 'NARIÑO', 169),
(35, 'N . DE SANTANDER', 169),
(37, 'QUINDIO', 169),
(39, 'RISARALDA', 169),
(41, 'SANTANDER', 169),
(43, 'SUCRE', 169),
(45, 'TOLIMA', 169),
(47, 'VALLE DEL CAUCA', 169),
(49, 'ARAUCA', 169);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Empresa`
--

CREATE TABLE `Empresa` (
  `nitEmpresa` bigint(20) NOT NULL,
  `Nombre` longtext,
  `numeroEmpleados` varchar(45) DEFAULT NULL,
  `Direccion` longtext,
  `Pagina` varchar(45) DEFAULT NULL,
  `Sector_idSector` int(11) NOT NULL,
  `Tipologia_idTipologia` int(11) NOT NULL,
  `Pais_idPais` int(11) NOT NULL,
  `Departamento_idDepartamento` int(11) NOT NULL,
  `Ciudad_idCiudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FirmaContrato`
--

CREATE TABLE `FirmaContrato` (
  `Documento` bigint(20) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellidos` varchar(45) DEFAULT NULL,
  `FirmaContrato` varchar(45) DEFAULT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL,
  `Empresa_nitEmpresa` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pais`
--

CREATE TABLE `Pais` (
  `idPais` int(11) NOT NULL,
  `Pais` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Pais`
--

INSERT INTO `Pais` (`idPais`, `Pais`) VALUES
(13, 'AFGANISTAN'),
(17, 'ALBANIA'),
(23, 'ALEMANIA'),
(26, 'ARMENIA'),
(27, 'ARUBA'),
(29, 'BOSNIA Y HERZEGOVINA'),
(31, 'BURKINA FASO'),
(37, 'ANDORRA'),
(40, 'ANGOLA'),
(41, 'ANGUILA'),
(43, 'ANTIGUA Y BARBUDA'),
(53, 'ARABIA SAUDITA'),
(59, 'ARGELIA'),
(63, 'ARGENTINA'),
(69, 'AUSTRALIA'),
(72, 'AUSTRIA'),
(74, 'AZERBAIYAN'),
(77, 'BAHAMAS'),
(80, 'BAREIN'),
(81, 'BANGLADES'),
(83, 'BARBADOS'),
(87, 'BELGICA'),
(88, 'BELICE'),
(90, 'BERMUDAS'),
(91, 'BIELORRUSIA'),
(93, 'BIRMANIA'),
(97, 'ESTADO PLURINACIONAL DE BOLIVIA'),
(101, 'BOTSUANA'),
(105, 'BRASIL'),
(108, 'BRUNEI'),
(111, 'BULGARIA'),
(115, 'BURUNDI'),
(119, 'BUTAN'),
(127, 'CABO VERDE'),
(137, 'ISLAS CAIMAN'),
(141, 'CAMBOYA'),
(145, 'CAMERUN'),
(149, 'CANADA'),
(159, 'CIUDAD DEL VATICANO'),
(165, 'ISLAS COCOS'),
(169, 'COLOMBIA'),
(173, 'COMORAS'),
(177, 'REPUBLICA DEL CONGO'),
(183, 'ISLAS COOK'),
(187, 'REPUBLICA DEMOCRATICA '),
(190, 'REPUBLICA DE'),
(193, 'COSTA DE MARFIL'),
(196, 'COSTA RICA'),
(198, 'CROACIA'),
(199, 'CUBA'),
(203, 'CHAD'),
(211, 'CHILE'),
(215, 'CHINA'),
(218, 'PROVINCIA DE CHINA'),
(221, 'CHIPRE'),
(229, 'BENIN'),
(232, 'DINAMARCA'),
(235, 'DOMINICA'),
(239, 'ECUADOR'),
(240, 'EGIPTO'),
(242, 'EL SALVADOR'),
(243, 'ERITREA'),
(244, 'EMIRATOS ARABES UNIDOS'),
(245, 'ESPA�A'),
(246, 'ESLOVAQUIA'),
(247, 'ESLOVENIA'),
(249, 'ESTADOS UNIDOS'),
(251, 'ESTONIA'),
(253, 'ETIOPIA'),
(259, 'ISLAS FEROE'),
(267, 'FILIPINAS'),
(271, 'FINLANDIA'),
(275, 'FRANCIA'),
(281, 'GABON'),
(285, 'GAMBIA'),
(287, 'GEORGIA'),
(289, 'GHANA'),
(293, 'GIBRALTAR'),
(297, 'GRANADA'),
(301, 'GRECIA'),
(305, 'GROENLANDIA'),
(309, 'GUADALUPE'),
(313, 'GUAM'),
(317, 'GUATEMALA'),
(325, 'GUAYANA FRANCESA'),
(329, 'GUINEA'),
(331, 'GUINEA ECUATORIAL'),
(334, 'GUINEA-BISAU'),
(337, 'GUYANA'),
(341, 'HAITI'),
(345, 'HONDURAS'),
(351, 'HONG KONG'),
(355, 'HUNGRIA'),
(361, 'INDIA'),
(365, 'INDONESIA'),
(369, 'IRAK'),
(372, 'REPUBLICA ISLAMICA DE'),
(375, 'IRLANDA'),
(379, 'ISLANDIA'),
(383, 'ISRAEL'),
(386, 'ITALIA'),
(391, 'JAMAICA'),
(399, 'JAPON'),
(403, 'JORDANIA'),
(406, 'KAZAJISTAN'),
(410, 'KENIA'),
(411, 'KIRIBATI'),
(412, 'KIRGUISTAN'),
(413, 'KUWAIT'),
(420, 'REPUBLICA DEMOCRATICA POPULAR'),
(426, 'LESOTO'),
(429, 'LETONIA'),
(431, 'LIBANO'),
(434, 'LIBERIA'),
(438, 'LIBIA'),
(440, 'LIECHTENSTEIN'),
(443, 'LITUANIA'),
(445, 'LUXEMBURGO'),
(447, 'MACAO'),
(448, 'LA ANTIGUA REPUBLICA YUGOSLAVADE'),
(450, 'MADAGASCAR'),
(455, 'MALASIA'),
(458, 'MALAUI'),
(461, 'MALDIVAS'),
(464, 'MALI'),
(467, 'MALTA'),
(469, 'ISLAS MARIANAS DEL NORTE'),
(472, 'ISLAS MARSHALL'),
(474, 'MARRUECOS'),
(477, 'MARTINICA'),
(485, 'MAURICIO'),
(488, 'MAURITANIA'),
(493, 'MEXICO'),
(494, 'ESTADOS FEDERADOS DE'),
(496, 'REPUBLICA DE'),
(497, 'MONGOLIA'),
(498, 'MONACO'),
(501, 'MONTSERRAT'),
(505, 'MOZAMBIQUE'),
(507, 'NAMIBIA'),
(508, 'NAURU'),
(511, 'ISLA DE NAVIDAD'),
(517, 'NEPAL'),
(521, 'NICARAGUA'),
(525, 'NIGER'),
(528, 'NIGERIA'),
(531, 'NIUE'),
(535, 'NORFOLK'),
(538, 'NORUEGA'),
(542, 'NUEVA CALEDONIA'),
(545, 'PAPUA NUEVA GUINEA'),
(548, 'NUEVA ZELANDA'),
(551, 'VANUATU'),
(556, 'OMAN'),
(566, 'ISLAS ULTRAMARINAS DE ESTADOS UNIDOS'),
(573, 'PAISES BAJOS'),
(576, 'PAKISTAN'),
(578, 'PALAOS'),
(579, 'ESTADO DE'),
(580, 'PANAMA'),
(586, 'PARAGUAY'),
(589, 'PERU'),
(593, 'ISLAS PITCAIRN'),
(599, 'POLINESIA FRANCESA'),
(603, 'POLONIA'),
(607, 'PORTUGAL'),
(611, 'PUERTO RICO'),
(618, 'CATAR'),
(628, 'REINO UNIDO'),
(640, 'REPUBLICA CENTROAFRICANA'),
(644, 'REPUBLICA CHECA'),
(647, 'REPUBLICA DOMINICANA'),
(660, 'REUNION'),
(665, 'ZIMBABUE'),
(670, 'RUMANIA'),
(675, 'RUANDA'),
(676, 'FEDERACION DE'),
(677, 'ISLAS SALOMON'),
(685, 'SUDAN'),
(687, 'SAMOA'),
(690, 'SAMOA AMERICANA'),
(695, 'SAN CRISTOBAL Y NIEVES'),
(697, 'SAN MARINO'),
(700, 'SAN PEDRO Y MIQUELON'),
(705, 'SAN VICENTE Y LAS GRANADINAS'),
(710, 'ASCENSION Y TRISTAN DE ACU�A'),
(715, 'SANTA LUCIA'),
(720, 'SANTO TOME Y PRINCIPE'),
(728, 'SENEGAL'),
(731, 'SEYCHELLES'),
(735, 'SIERRA LEONA'),
(741, 'SINGAPUR'),
(744, 'REPUBLICA ARABE'),
(748, 'SOMALIA'),
(750, 'SRI LANKA'),
(756, 'SUDAFRICA'),
(759, 'REPUBLICA ARABE SAHARAUI DEMOCRATICA'),
(764, 'SUECIA'),
(767, 'SUIZA'),
(770, 'SURINAM'),
(773, 'SUAZILANDIA'),
(774, 'TAYIKISTAN'),
(776, 'TAILANDIA'),
(780, 'REPUBLICA UNIDA DE'),
(783, 'YIBUTI'),
(787, 'TERRITORIO BRITANICO DEL OCEANO INDICO'),
(788, 'TIMOR ORIENTAL'),
(800, 'TOGO'),
(805, 'TOKELAU'),
(810, 'TONGA'),
(815, 'TRINIDAD Y TOBAGO'),
(820, 'TUNEZ'),
(823, 'ISLAS TURCAS Y CAICOS'),
(825, 'TURKMENISTAN'),
(827, 'TURQUIA'),
(828, 'TUVALU'),
(830, 'UCRANIA'),
(833, 'UGANDA'),
(845, 'URUGUAY'),
(847, 'UZBEKISTAN'),
(850, 'REPUBLICA BOLIVARIANA DE'),
(855, 'VIETNAM'),
(863, 'ISLAS VIRGENES BRITANICAS'),
(866, 'ISLAS VIRGENES DE LOS ESTADOS UNIDOS'),
(870, 'FIYI'),
(875, 'WALLIS Y FUTUNA'),
(880, 'YEMEN'),
(885, 'YUGOSLAVIA'),
(888, 'REPUBLICA DEMOCRATICA DEL'),
(890, 'ZAMBIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Parametros`
--

CREATE TABLE `Parametros` (
  `idParametros` int(11) NOT NULL,
  `Parametros` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Persona`
--

CREATE TABLE `Persona` (
  `idPersona` bigint(20) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Apellido` varchar(45) DEFAULT NULL,
  `Direccion` longtext,
  `FirmaContratoPersona` longtext,
  `Profesion_idProfesion` int(11) NOT NULL,
  `Pais_idPais` int(11) NOT NULL,
  `Departamento_idDepartamento` int(11) NOT NULL,
  `Ciudad_idCiudad` int(11) NOT NULL,
  `TipoDocumento_idTipoDocumento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Persona`
--

INSERT INTO `Persona` (`idPersona`, `Nombre`, `Apellido`, `Direccion`, `FirmaContratoPersona`, `Profesion_idProfesion`, `Pais_idPais`, `Departamento_idDepartamento`, `Ciudad_idCiudad`, `TipoDocumento_idTipoDocumento`) VALUES
(1019042184, 'Luis Daniel', 'Gordo Navas', 'Calle 2 #93D - 30', 'Luis Danniel Gordo Navas', 3, 169, 5, 149, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Profesion`
--

CREATE TABLE `Profesion` (
  `idProfesion` int(11) NOT NULL,
  `Profesion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Profesion`
--

INSERT INTO `Profesion` (`idProfesion`, `Profesion`) VALUES
(1, 'Desarrollador Junior'),
(2, 'Desarrollador Medio'),
(3, 'Desarrollador Senior'),
(4, 'QA Junior'),
(5, 'QA medio'),
(6, 'QA Senior'),
(7, 'Recursos humanos'),
(8, 'Administrativo'),
(9, 'Servicios generales'),
(10, 'domiciliario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Sector`
--

CREATE TABLE `Sector` (
  `idSector` int(11) NOT NULL,
  `Sector` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Sector`
--

INSERT INTO `Sector` (`idSector`, `Sector`) VALUES
(1, 'Publicidad / RRPP'),
(2, 'Agricultura / Pesca / Ganadería'),
(3, 'Informática / Hardware'),
(4, 'Informática / Software'),
(5, 'Construcción / obras'),
(6, 'Venta al consumidor'),
(7, 'Educación'),
(8, 'Energía'),
(9, 'Entretenimiento / Deportes'),
(10, 'Finanzas / Banca'),
(11, 'Salud / Medicina'),
(12, 'Hostelería / Turismo'),
(13, 'Internet'),
(14, 'Medios de Comunicación'),
(15, 'Fabricación'),
(16, 'Gobierno / No Lucro'),
(17, 'Servicios Profesionales'),
(18, 'Materias Primas'),
(19, 'RRHH / Personal'),
(20, 'Venta al por mayor'),
(21, 'Telecomunicaciones'),
(22, 'Transporte'),
(23, 'Legal / Asesoría');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoContrato`
--

CREATE TABLE `TipoContrato` (
  `idTipoContrato` int(11) NOT NULL,
  `TipoContrato` longtext,
  `Descripcionl` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TipoDocumento`
--

CREATE TABLE `TipoDocumento` (
  `idTipoDocumento` int(11) NOT NULL,
  `TipoDocumento` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `TipoDocumento`
--

INSERT INTO `TipoDocumento` (`idTipoDocumento`, `TipoDocumento`) VALUES
(1, 'Cedula de ciudadania'),
(2, 'Cedula de extranjeria'),
(3, 'Pasaporte');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tipologia`
--

CREATE TABLE `Tipologia` (
  `idTipologia` int(11) NOT NULL,
  `Tipologia` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Tipologia`
--

INSERT INTO `Tipologia` (`idTipologia`, `Tipologia`) VALUES
(1, 'Agencia de reclutamiento'),
(2, 'Empleador directo'),
(3, 'Servicios temporales');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `AlmacenContratos`
--
ALTER TABLE `AlmacenContratos`
  ADD PRIMARY KEY (`idAlmacenContratos`),
  ADD KEY `fk_AlmacenContratos_TipoContrato1_idx` (`TipoContrato_idTipoContrato`);

--
-- Indices de la tabla `Cargo`
--
ALTER TABLE `Cargo`
  ADD PRIMARY KEY (`idCargo`);

--
-- Indices de la tabla `Ciudad`
--
ALTER TABLE `Ciudad`
  ADD PRIMARY KEY (`idCiudad`),
  ADD KEY `fk_Ciudad_Departamento1_idx` (`Departamento_idDepartamento`);

--
-- Indices de la tabla `Clausulas`
--
ALTER TABLE `Clausulas`
  ADD PRIMARY KEY (`idClausulas`);

--
-- Indices de la tabla `Contacto`
--
ALTER TABLE `Contacto`
  ADD PRIMARY KEY (`IdContacto`),
  ADD KEY `fk_Contacto_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`),
  ADD KEY `fk_Contacto_Cargo1_idx` (`Cargo_idCargo`),
  ADD KEY `fk_Contacto_Empresa1_idx` (`Empresa_nitEmpresa`);

--
-- Indices de la tabla `Contratos`
--
ALTER TABLE `Contratos`
  ADD PRIMARY KEY (`idContratos`),
  ADD KEY `fk_Contratos_TipoContrato1_idx` (`TipoContrato_idTipoContrato`),
  ADD KEY `fk_Contratos_FirmaContrato1_idx` (`FirmaContrato_Documento`),
  ADD KEY `fk_Contratos_Empresa1_idx` (`Empresa_nitEmpresa`),
  ADD KEY `fk_Contratos_Persona1_idx` (`Persona_idPersona`);

--
-- Indices de la tabla `ContratosHasClausulas`
--
ALTER TABLE `ContratosHasClausulas`
  ADD PRIMARY KEY (`Contratos_idContratos`,`Clausulas_idClausulas`),
  ADD KEY `fk_Contratos_has_Clausulas_Clausulas1_idx` (`Clausulas_idClausulas`),
  ADD KEY `fk_Contratos_has_Clausulas_Contratos1_idx` (`Contratos_idContratos`);

--
-- Indices de la tabla `ContratosHasParametros`
--
ALTER TABLE `ContratosHasParametros`
  ADD PRIMARY KEY (`Contratos_idContratos`,`Parametros_idParametros`),
  ADD KEY `fk_Contratos_has_Parametros_Parametros1_idx` (`Parametros_idParametros`),
  ADD KEY `fk_Contratos_has_Parametros_Contratos1_idx` (`Contratos_idContratos`);

--
-- Indices de la tabla `Departamento`
--
ALTER TABLE `Departamento`
  ADD PRIMARY KEY (`idDepartamento`),
  ADD KEY `fk_Departamento_Pais_idx` (`Pais_idPais`);

--
-- Indices de la tabla `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`nitEmpresa`),
  ADD KEY `fk_Empresa_Sector1_idx` (`Sector_idSector`),
  ADD KEY `fk_Empresa_Tipologia1_idx` (`Tipologia_idTipologia`),
  ADD KEY `fk_Empresa_Pais1_idx` (`Pais_idPais`),
  ADD KEY `fk_Empresa_Departamento1_idx` (`Departamento_idDepartamento`),
  ADD KEY `fk_Empresa_Ciudad1_idx` (`Ciudad_idCiudad`);

--
-- Indices de la tabla `FirmaContrato`
--
ALTER TABLE `FirmaContrato`
  ADD PRIMARY KEY (`Documento`),
  ADD KEY `fk_FirmaContrato_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`),
  ADD KEY `fk_FirmaContrato_Empresa1_idx` (`Empresa_nitEmpresa`);

--
-- Indices de la tabla `Pais`
--
ALTER TABLE `Pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Indices de la tabla `Parametros`
--
ALTER TABLE `Parametros`
  ADD PRIMARY KEY (`idParametros`);

--
-- Indices de la tabla `Persona`
--
ALTER TABLE `Persona`
  ADD PRIMARY KEY (`idPersona`),
  ADD KEY `fk_Persona_Profesion1_idx` (`Profesion_idProfesion`),
  ADD KEY `fk_Persona_Pais1_idx` (`Pais_idPais`),
  ADD KEY `fk_Persona_Departamento1_idx` (`Departamento_idDepartamento`),
  ADD KEY `fk_Persona_Ciudad1_idx` (`Ciudad_idCiudad`),
  ADD KEY `fk_Persona_TipoDocumento1_idx` (`TipoDocumento_idTipoDocumento`);

--
-- Indices de la tabla `Profesion`
--
ALTER TABLE `Profesion`
  ADD PRIMARY KEY (`idProfesion`);

--
-- Indices de la tabla `Sector`
--
ALTER TABLE `Sector`
  ADD PRIMARY KEY (`idSector`);

--
-- Indices de la tabla `TipoContrato`
--
ALTER TABLE `TipoContrato`
  ADD PRIMARY KEY (`idTipoContrato`);

--
-- Indices de la tabla `TipoDocumento`
--
ALTER TABLE `TipoDocumento`
  ADD PRIMARY KEY (`idTipoDocumento`);

--
-- Indices de la tabla `Tipologia`
--
ALTER TABLE `Tipologia`
  ADD PRIMARY KEY (`idTipologia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Clausulas`
--
ALTER TABLE `Clausulas`
  MODIFY `idClausulas` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Sector`
--
ALTER TABLE `Sector`
  MODIFY `idSector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `TipoContrato`
--
ALTER TABLE `TipoContrato`
  MODIFY `idTipoContrato` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `AlmacenContratos`
--
ALTER TABLE `AlmacenContratos`
  ADD CONSTRAINT `fk_AlmacenContratos_TipoContrato1` FOREIGN KEY (`TipoContrato_idTipoContrato`) REFERENCES `TipoContrato` (`idTipoContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Ciudad`
--
ALTER TABLE `Ciudad`
  ADD CONSTRAINT `fk_Ciudad_Departamento1` FOREIGN KEY (`Departamento_idDepartamento`) REFERENCES `Departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Contacto`
--
ALTER TABLE `Contacto`
  ADD CONSTRAINT `fk_Contacto_Cargo1` FOREIGN KEY (`Cargo_idCargo`) REFERENCES `Cargo` (`idCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contacto_Empresa1` FOREIGN KEY (`Empresa_nitEmpresa`) REFERENCES `Empresa` (`nitEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contacto_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Contratos`
--
ALTER TABLE `Contratos`
  ADD CONSTRAINT `fk_Contratos_Empresa1` FOREIGN KEY (`Empresa_nitEmpresa`) REFERENCES `Empresa` (`nitEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_FirmaContrato1` FOREIGN KEY (`FirmaContrato_Documento`) REFERENCES `FirmaContrato` (`Documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_Persona1` FOREIGN KEY (`Persona_idPersona`) REFERENCES `Persona` (`idPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_TipoContrato1` FOREIGN KEY (`TipoContrato_idTipoContrato`) REFERENCES `TipoContrato` (`idTipoContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ContratosHasClausulas`
--
ALTER TABLE `ContratosHasClausulas`
  ADD CONSTRAINT `fk_Contratos_has_Clausulas_Clausulas1` FOREIGN KEY (`Clausulas_idClausulas`) REFERENCES `Clausulas` (`idClausulas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_has_Clausulas_Contratos1` FOREIGN KEY (`Contratos_idContratos`) REFERENCES `Contratos` (`idContratos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ContratosHasParametros`
--
ALTER TABLE `ContratosHasParametros`
  ADD CONSTRAINT `fk_Contratos_has_Parametros_Contratos1` FOREIGN KEY (`Contratos_idContratos`) REFERENCES `Contratos` (`idContratos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contratos_has_Parametros_Parametros1` FOREIGN KEY (`Parametros_idParametros`) REFERENCES `Parametros` (`idParametros`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Departamento`
--
ALTER TABLE `Departamento`
  ADD CONSTRAINT `fk_Departamento_Pais` FOREIGN KEY (`Pais_idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Empresa`
--
ALTER TABLE `Empresa`
  ADD CONSTRAINT `fk_Empresa_Ciudad1` FOREIGN KEY (`Ciudad_idCiudad`) REFERENCES `Ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Departamento1` FOREIGN KEY (`Departamento_idDepartamento`) REFERENCES `Departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Pais1` FOREIGN KEY (`Pais_idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Sector1` FOREIGN KEY (`Sector_idSector`) REFERENCES `Sector` (`idSector`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empresa_Tipologia1` FOREIGN KEY (`Tipologia_idTipologia`) REFERENCES `Tipologia` (`idTipologia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `FirmaContrato`
--
ALTER TABLE `FirmaContrato`
  ADD CONSTRAINT `fk_FirmaContrato_Empresa1` FOREIGN KEY (`Empresa_nitEmpresa`) REFERENCES `Empresa` (`nitEmpresa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_FirmaContrato_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Persona`
--
ALTER TABLE `Persona`
  ADD CONSTRAINT `fk_Persona_Ciudad1` FOREIGN KEY (`Ciudad_idCiudad`) REFERENCES `Ciudad` (`idCiudad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_Departamento1` FOREIGN KEY (`Departamento_idDepartamento`) REFERENCES `Departamento` (`idDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_Pais1` FOREIGN KEY (`Pais_idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_Profesion1` FOREIGN KEY (`Profesion_idProfesion`) REFERENCES `Profesion` (`idProfesion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Persona_TipoDocumento1` FOREIGN KEY (`TipoDocumento_idTipoDocumento`) REFERENCES `TipoDocumento` (`idTipoDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
